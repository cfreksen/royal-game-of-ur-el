;;; royal-game-of-ur.el --- An Elisp implementation of the Royal Game of Ur  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Casper Freksen <casper@freksen.dk>

;; Author: Casper Freksen  <casper@freksen.dk>
;; Maintainer: Casper Freksen  <casper@freksen.dk>
;; Homepage: https://gitlab.com/cfreksen/royal-game-of-ur-el
;; Keywords: games

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary

;; This is an Elisp implementation of the Royal Game of Ur, which is
;; an ancient game from around 2500 BC.

;;; Code:

(defconst rgou-init-board
  '(((0 0 0 0) (0 0 0 0)) (0 0 0 0 0 0 0 0) ((0 0 0) (0 0 0)))
  "The initial board layout")

(defvar rgou-init-score
  '((7 0) (7 0)))

(defvar rgou-current-board
  rgou-init-board
  "The board of the current game")

(defvar rgou-current-score
  rgou-init-score)

(defvar rgou-current-player-turn
  1
  "The current player of the current game")

(defun rgou-good-move-p (to board player)
  "TODO"
  (cond ((<= to 4)
         ;; In first home zone
         (let ((curr-on-to (nth (1- to) (nth (1- player) (nth 0 board)))))
           (= 0 curr-on-to)))
        ((<= to 12)
         ;; In middle lane
         (let ((curr-on-to (nth (- to 5) (nth 1 board))))
           (not (= player curr-on-to))))
        ((<= to 14)
         ;; In final home zone
         (let ((curr-on-to (nth (- to 13) (nth (1- player) (nth 2 board)))))
           (= 0 curr-on-to)))
        ((= to 15)
         ;; Goal
         t)
        (t
         ;; Out of the board
         nil)))


(defun rgou-write-in-list (pos lst color)
  ""
  (letrec ((aux (lambda (cnt lstm)
                  (cond ((= cnt 0)
                         (cons color (cdr lstm)))
                        ((eq lstm nil)
                         (error "Index out of bounds when writing to list"))
                        (t
                         (cons (car lstm)
                               (funcall aux (1- cnt) (cdr lstm))))))))
    (funcall aux pos lst)))

(defun rgou-write-on-board (pos board player color)
  "TODO"
  (let ((head (nth 0 board))
        (mid (nth 1 board))
        (tail (nth 2 board)))
    (cond ((<= pos 0)
           (error "Tried to write before the board"))
          ((<= pos 4)
           ;; In first home zone
           (cons (if (= player 1)
                     (list (rgou-write-in-list (1- pos) (nth 0 head) color)
                           (nth 1 head))
                   (list (nth 0 head)
                         (rgou-write-in-list (1- pos) (nth 1 head) color)))
                 (cdr board)))
          ((<= to 12)
           ;; In middle lane
           (list head
                 (rgou-write-in-list (- pos 5) mid color)
                 tail))
          ((<= to 14)
           ;; In final home zone
           (list head
                 mid
                 (if (= player 1)
                     (list (rgou-write-in-list (- pos 13) (nth 0 tail) color)
                           (nth 1 tail))
                   (list (nth 0 tail)
                         (rgou-write-in-list (- pos 13) (nth 1 tail) color)))))
          ((= to 15)
           ;; Goal
           board)
          (t
           ;; Out of the board
           (error "Tried to write out of the board")))))

(defun rgou-make-move (from delta board player)
  "TODO"
  (when (rgou-good-move-p ((+ from delta) board player))
    (error "TODO")))

(defun rgou-inc-score (player score)
  "TODO"
  (let ((p1s (nth 0 score))
        (p2s (nth 1 score)))
    (if (= player 1)
        (list (list (car p1s) (1+ (cadr p1s)))
              p2s)
      (list p1s
            (list (car p2s) (1+ (cadr p2s)))))))

(defun rgou-add-home (player score delta)
  "TODO"
  (let ((p1s (nth 0 score))
        (p2s (nth 1 score)))
    (if (= player 1)
        (list (cons (car (+ delta p1s) (cdr p1s)))
              p2s)
      (list p1s
            (cons (car (= delta p2s) (cdr p2s)))))))


;; Displaying state

(defvar rgou-board-origo
  '(0 0)
  "The line and column of the upper left corner of the board")

(defconst rgou-board-skeleton
  (concat "+---+---+---+---+       +---+---+\n"
          "|* *|   |   |   |       |* *|   |\n"
          "+---+---+---+---+---+---+---+---+\n"
          "|   |   |   |* *|   |   |   |   |\n"
          "+---+---+---+---+---+---+---+---+\n"
          "|* *|   |   |   |       |* *|   |\n"
          "+---+---+---+---+       +---+---+\n"))

(defconst rgou-cell-width
  4)

(defconst rgou-cell-height
  2)

(defconst rgou-cell-middle-width
  2)

(defconst rgou-cell-middle-height
  1)

(defun rgou-move-point-to-pos (pos player)
  "TODO"
  (let ((ol (nth 0 rgou-board-origo))
        (oc (nth 1 rgou-board-origo)))
    ;; First we need to find the correct line
    ;; Goto origo of board
    (goto-char (point-min))
    (forward-line ol)
    ;; Move into center of a cell
    (forward-line rgou-cell-middle-height)
    ;; Navigate inside board.
    (cond ((or (<= pos 4)
               (> pos 12))
           ;; Either home zone
           (forward-line (* 2 (1- player) rgou-cell-height)))
          (t
           ;; Middle lane
           (forward-line rgou-cell-height)))
    ;; Secondly, navigate to the correct column
    ;; Goto origo of board
    (forward-char oc)
    ;; Move into center of a cell
    (forward-char rgou-cell-middle-width)
    ;; Navigate inside board.
    (cond ((<= pos 4)
           ;; First home zone. Right to left.
           (forward-char (* rgou-cell-width (- 4 pos))))
          ((<= pos 12)
           ;; Middle lane. Left to right.
           (forward-char (* rgou-cell-width (- pos 5))))
          (t
           ;; Final home zone. Right to left
           (forward-char (* rgou-cell-width (- 20 pos)))))))

(defconst rgou-score-header-line-pos
  9
  "TODO")

(defvar rgou-score-content-line-pos
  11
  "TODO")

(defconst rgou-score-header
  (concat "     Player 1       Player 2\n"
          "  Home     Goal   Home    Goal\n")
  "TODO")

(defconst rgou-score-content-format
  "     %d        %d      %d       %d"
  "TODO")

(defun rgou-display-score-header (header)
  "TODO"
  (progn
    (goto-char (point-min))
    (forward-line rgou-score-header-line-pos)
    (insert header)
    (setq rgou-score-content-line-pos (1- (line-number-at-pos)))))


(defun rgou-display-score-content (score)
  "TODO"
  (let* ((p1s (nth 0 score))
         (p2s (nth 1 score))
         (p1h (nth 0 p1s))
         (p1g (nth 1 p1s))
         (p2h (nth 0 p2s))
         (p2g (nth 1 p2s)))
    (goto-char (point-min))
    (forward-line rgou-score-content-line-pos)
    ;; Clear line, but do not remove newline
    (insert "K")
    (forward-char -1)
    (kill-line)
    (insert (format rgou-score-content-format p1h p1g p2h p2g))))


;;; royal-game-of-ur.el ends here
